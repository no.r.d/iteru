#!/usr/bin/env python3
"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""


from glob import glob
from os.path import splitext, basename
from setuptools import setup, find_namespace_packages

setup(name='nord-iteru',
      packages=find_namespace_packages('src', include=['nord.*']),
      version='0.0.1',
      description='nord typecasting extension / space',
      author='Nate Byrnes',
      author_email='nate@qabal.org',
      url='http://gitlab.com/no.r.d/iteru/',
      install_requires=['nord-nord', 'nord-sigurd'],
      include_package_data=True,
      package_data={
          "": ["config/*.conf", "models/*.egg"]
      },
      package_dir={'': 'src'},
      py_modules=[splitext(basename(path))[0] for path in glob('src/*.py')],
      python_requires='>=3.7',
      classifiers=[
          "Programming Language :: Python :: 3",
          "License :: OSI Approved :: AGPLv3",
          "Operating System ::  OS Independent"
      ],
      license="AGPLv3",
      platform="OS Independent")
