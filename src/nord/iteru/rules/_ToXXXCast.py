"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
Boilerplate module used for name sake.
"""

import nord.nord.Rule as Rule
import sys


class ToXXXUse(Rule):
    """Namesake class facilitating named type to convert edges."""

    def apply(self):
        """
        Employ the cast function from the child instance's source node.

        This is somewhat implicit as the Rule mechanism will only
        allow the correct node type to connect. So, the inheriting rules
        really do nothing but provide the names allowing the alignment
        of the node types to the conversion node.
        """
        self.edge.set_cargo(self.edge.get_source().cast)
        return True


sys.modules[__name__] = ToXXXUse
