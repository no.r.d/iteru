"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
Convert the passed data and continue the assignment.
"""

import nord.nord.Rule as Rule
from nord.nord.exceptions import RuntimeException as RuntimeException
import sys


class ConvertAssign(Rule):
    """Apply the requested conversion to the passed data and hands it over."""

    def apply(self):
        """Add the source container's contents to the target containers namespace."""
        if self.edge.get_source().has_cargo():
            self.edge.get_source().set(self.edge.get_source().get_cargo())
            self.edge.set_cargo(self.edge.get_source().get())
            return True
        else:
            raise RuntimeException(
                _("Converter asked to assign, but has nothing to assign: {}")
                .format(self.edge.get_source())
            )


sys.modules[__name__] = ConvertAssign
