"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Rule as Rule
import sys

class ContainsConvert(Rule):
    """
    The ContainsConvert rule places the Data within the current container's memory space with the runtime
    for subsequent use
    """

    def apply(s):
        """
        Adds the Target Data node to the runtime's current container's namespace for subsequent usage

        The target node is assigned in the Rule constructor as is the runtime
        The current node in the runtime is the source Node (of the edge to the target)
        """
        return True

sys.modules[__name__] = ContainsConvert