"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
Pass the value to be converted to the Converter.
"""

import nord.nord.Rule as Rule
import sys


class PassConvert(Rule):
    """Pass the value to the converter."""

    def apply(self):
        """Apply the rule to this end of the edge."""
        if hasattr(self.edge.get_target(), "set_cargo"):
            self.edge.get_target().set_cargo(self.edge.get_cargo())
            return True
        else:  # pragma: no cover ## So far the only way I've found to test this is to put a deliberately incorrect rule module inplace.
            raise Exception(
                _("Attempted to assign a value to a non-assignable node: {} -> {}")
                .format(str(self.source), str(self.target))
            )


sys.modules[__name__] = PassConvert
