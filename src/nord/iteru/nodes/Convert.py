"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.nord.Node as Node
import nord.nord.Property as Property
import sys


_mapping = {
    "str": str,
    "int": int,
    "float": float
}


class Convert(Node):
    """"""

    def __init__(s, graph):
        super().__init__(graph)
        s._properties = {
            'typename': Property(s, 'typename', description=_("""The name of the known datatype to convert the input data to."""))
        }

    def set_converter(self, func):
        """Set the conversion function."""
        self._converter = func

    def get_converter(self):
        """Return the conversion function."""
        if hasattr(self, '_converter'):
            return self._converter
        elif self.is_set_property('typename'):
            return _mapping.get(self.get_property('typename'), lambda x: x)
        else:
            return lambda x: x

    def set(self, value):
        """Set the value to be converted."""
        self.value = value

    def get(self):
        """Return the converted value."""
        return self.get_converter()(self.value)

    def isset(self):
        """Return True if a value has been set for this converter."""
        return hasattr(self, 'value')

    def set_cargo(self, value):
        """Set the value to be canverted as cargo."""
        self._cargo = value

    def has_cargo(self):
        """Return true if _cargo has been set."""
        return hasattr(self, '_cargo')

    def get_cargo(self):
        """Return the cargo if set, otherwise, raise exception."""
        return self._cargo


sys.modules[__name__] = Convert
